"use strict";
const jwt = require("jsonwebtoken");
const { ObjectID } = require("mongodb");
const { getInstance } = require("../db");

const login = async (req, res) => {
  try {
    if (!req.body.username) {
      res.status(400).json({ success: false, error: "missing username" });
      return;
    }
    if (req.body.username.length < 3) {
      res.status(400).json({ success: false, error: "invalid username" });
      return;
    }
    if (!req.body.pass || req.body.pass.length < 8) {
      res.status(400).json({ success: false, error: "invalid pass" });
      return;
    }
    const db = await getInstance();
    const users = db.collection("users");
    const user = await users.findOne({
      username: req.body.username,
      pass: req.body.pass,
    });
    if (!user) {
      res.status(403).json({ success: false });
      return;
    }
    const resp = await generateToken(user._id);
    res.set("Set-Cookie", `refresh_token=${resp.refresh_token}`)
    res.json(resp);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};
const register = async (req, res) => {
  try {
    if (!req.body.username) {
      res.status(400).json({ success: false, error: "missing username" });
      return;
    }
    if (req.body.username.length < 3) {
      res.status(400).json({ success: false, error: "invalid username" });
    }
    if (
      !req.body.pass ||
      req.body.pass.length < 8 ||
      !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(req.body.pass)
    ) {
      res.status(400).json({ success: false, error: "invalid pass" });
      return;
    }
    const db = await getInstance();
    const users = db.collection("users");
    const ok = await users.insertOne({
      username: req.body.username,
      pass: req.body.pass,
      email: req.body.email || "",
    });
    if (!ok) {
      res.status(500).json({ success: false });
      return;
    }
    console.log("INFO:", "register ok");
    res.json({ success: true });
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

const refreshToken = async (req, res) => {
  try {
    const token = req.cookies.refresh_token;
    if (!token) {
      console.error("token not found");
      res.status(401).json({ success: false });
      return;
    }
    const db = await getInstance();
    const users = db.collection("users");
    const decoded = jwt.verify(token, "40bil");
    const check_token = await users.findOne(
      { refreshToken: token }
    );
    if (check_token) {
      const ok = await generateToken(decoded.id);
      res.set("Set-Cookie", `refresh_token=${ok.refresh_token}`)
      res.json(ok);
    }
    return { "status": "refresh token invalid!" }



  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

const check = (req, res) => {
  res.json({ success: true });
};

const generateToken = async (userID) => {
  const accessToken = jwt.sign({ id: userID }, "30bil", { expiresIn: "15min" });
  const rToken = jwt.sign({ id: userID }, "40bil", { expiresIn: "10d" });
  const updateDoc = {
    $set: {
      refreshToken:
        rToken,
    },
  };
  const filter = { _id: ObjectID("5f7838b6e350b033bcbdf0ed") };
  const db = await getInstance();
  const users = db.collection("users");
  const ok = await users.updateOne(filter, updateDoc);
  if (!ok.result.nModified) {
    return { success: false };
  }
 
  return { success: true, access_token: accessToken, refresh_token: rToken };
};

module.exports = { login, register, refreshToken, check };
